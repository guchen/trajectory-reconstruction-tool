

from math import sqrt, pow
from .center import center
from .dist import dist
import numpy as N


def rg(pts):
    """Return the radius of gyration of mass of points.

    Radius of Gyration: https://en.wikipedia.org/wiki/Radius_of_gyration


    Args:
        pts: a list of points. Each point is in (latitude, longitude)

    Returns:
        the radius of gyration of points in kilometers

    """

    r_mean = center(pts)
    rg2 = N.mean([pow(dist(r_mean, pt), 2) for pt in pts])

    return sqrt(rg2)
