
from math import log
from numpy import argmin, arange


def entropy_rand(time_sequence):
    '''Random Entropy
    $S^{rand} = log_2 N$
    '''
    N = len(set(time_sequence))
    return log(N, 2)


def entropy_time_unc(time_sequence):
    '''Temproral-uncorrelated entropy
    $S^{unc} = - \sum_{i=1}^N p_i log_2 p_i$
    '''
    probabilities = {}
    length = len(time_sequence)
    for item in time_sequence:
        if probabilities.has_key(item):
            probabilities[item] += 1
        else:
            probabilities[item] = 1.0

    H = 0.0
    for item in time_sequence:
        p = probabilities[item] / length
        H += p * log(p, 2)

    return -H


def _lambda(time_sequence, pos):
    '''
    the length of the shortest substring starting at position i which doesn't previously appear from position 1 to i-1.
    '''
    n = len(time_sequence)
    SEP = '|*|'
    str_before = SEP.join([str(i) for i in time_sequence[:pos]])

    length = 0
    for end in range(pos + 1, n + 1):
        str_after = SEP.join([str(i) for i in time_sequence[pos:end]])
        if str_after in str_before:
            return length
        else:
            length += 1

    return length


def entropy_est(time_sequence):
    '''Estimated Entropy using the Lempel-Ziv data compression
    item in time_sequence should be able to str() uniformly.
    '''
    n = len(time_sequence)
    _sum = 0.0 + n
    for i in range(1, n):
        # print _lambda(time_sequence, i)
        _sum += _lambda(time_sequence, i)

    return log(n) / (_sum * 1.0 / n)


def _make_function(S, N):
    def func(x):
        H = -x * log(x, 2) - (1 - x) * log((1 - x), 2)
        return S - H - (1 - x) * log((N - 1), 2)
    return func


def predictability_max(S, N):
    _func = _make_function(S, N)
    xs = arange(0.001, 1, 0.001)
    ys = [_func(x)**2 for x in xs]
    return xs[argmin(ys)]

if __name__ == '__main__':
    H = [1 for x in range(0, 9999)]
    H[100] = 0
    print entropy_time_unc(H)
    print entropy_rand(H)
    print entropy_est(H)
    print predictability_max(log(100, 2), 100)
