import os
from math import log10


def load_user_data(location_path):
    """load subscriber's points:
    A subscriber named as bob should have locations, points and days, saved as
        location/bob.txt (should be given as location_path)
        point/bob.txt
        days/bob.txt

    output:
        points:
        locations:
        days:
        homeOrder: the id of the subscriber's home location if have, or -1
    """

    basename = os.path.basename(location_path)
    location_dir = os.path.dirname(location_path)
    pointPath = location_dir + "/../point/" + basename
    dayPath = location_dir + '/../days/' + basename

    days = 0
    with open(dayPath) as r:
        while 1:
            line = r.readline()
            if line:
                days += 1
            else:
                break

    homeOrder = -1
    locations = {}
    with open(location_path) as r:
        while 1:
            line = r.readline().replace("\n", "").replace("\r", "")
            if not line:
                break
            col = line.split('\t')
            locations[int(col[2])] = [float(col[0]),
                                      float(col[1])]  # (lat,lon)
            if int(col[4]) == 1:
                homeOrder = int(col[2])

    points = []
    with open(pointPath) as r:
        while 1:
            line = r.readline().replace("\n", "").replace("\r", "")
            if not line:
                break
            col = line.split('\t')
            loc = int(col[0])
            time = int(col[1]) / 3600.0
            vol = log10(int(col[2]))
            points.append([loc, time, vol])

    return (points, locations, homeOrder, days)
