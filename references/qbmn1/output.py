# output.py
# output methods

from scipy.io import savemat


def var_dump_mat(path, mdict):
    savemat(path, mdict, do_compression=True)


if __name__ == '__main__':
    var_dump_mat("./hello.mat", {"hi": [1, 2, 3], "OK": [1, 2]})
