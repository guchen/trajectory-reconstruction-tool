import gzip
import os
import pickle
import sys
from functools import partial
from multiprocessing import Pool

import numpy as np
import pandas as pd
from tqdm import tqdm, tqdm_notebook

from complete.util import dist, ls2p, p2ls, parse_timedelta
from sklearn.neighbors import NearestNeighbors

cache = {}
data_dir = os.path.dirname(os.path.realpath(__file__)) + '/../data'


class LocationDataMixin:
    def __init__(self, data):
        # data is pd.Dataframe, has 5 weekdays, temporal resolutions is 1 hour.
        data.columns = pd.DatetimeIndex(data.columns)
        losses = {}
        for C in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 30, 40, 50, 60]:
            _dt = pd.read_csv(
                data_dir + f'/losses/loss_indices_C{C}.call.txt.gz', index_col=0)
            losses[f'C{C}'] = [loss.tolist() for _, loss in _dt.iterrows()]

        self.data = data
        self.losses = losses
        self._users = data.index

    @property
    def users(self):
        if self._users is None:
            raise NotImplementedError
        else:
            return self._users

    def loss(self, traj, C):
        losses = self.losses[f'C{C}']
        trajs_loss = []
        for loss in losses:
            _traj = traj.copy()
            _traj.iloc[loss] = np.nan
            trajs_loss.append(_traj)
        return trajs_loss

    def trajectory(self, user):
        return self.trajectory_label(user)

    def _trajectory_label(self, user):
        return self.data.loc[user]

    def trajectory_label(self, user, consecutive=False, timediff=None):
        if user not in self._users:
            raise Exception('User does not exist')
        else:
            return self._trajectory_label(user)

    def trajectory_xy(self, user, consecutive=False, timediff=None):
        labels = self.trajectory_label(
            user, consecutive=consecutive, timediff=timediff)
        return self.traj2xys(labels)

    def traj2labels(self, traj):
        _traj = pd.Series(np.nan, index=traj.index, dtype=object)
        for i in range(len(traj.index)):
            x = traj.iloc[i]['x']
            y = traj.iloc[i]['y']
            if not np.isnan(x):
                _traj.iloc[i] = self.xy2label(x, y)
        return _traj

    def traj2xys(self, traj):
        return traj.apply(
            lambda label: pd.Series(self.label2xy(label) if not pd.isnull(label) else (np.nan, np.nan),
                                    index=['x', 'y']))

    def label2xy(self, label):
        return ls2p(label)

    def xy2label(self, x, y):
        return p2ls([x, y])

    def xy2towerxy(self, x, y, time=None):
        return [x, y]


class GranDataLocationData(LocationDataMixin):
    def __init__(self, D=60):
        path = data_dir + f'/grandata/data.txt.gz'
        data = pd.read_csv(path, index_col=0)

        self.radii = pd.read_csv(
            data_dir + f'/towers_pospago_xy_radii.txt', index_col=0, header=None, names=['radius'])
        self.weekly_appearance = pd.read_csv(
            data_dir + f'/towers_pospago_xy_week_hour.txt', index_col=0) + 1
        self.clf = None
        super(GranDataLocationData, self).__init__(data)

    def xy2towerxy(self, x, y, time):
        def _time_to_col(time):
            return '%.3d' % (time.weekday() * 100 + time.hour)

        if self.clf is None:
            self.points = self.radii.index.to_series().apply(self.label2xy).values.tolist()
            self.clf = NearestNeighbors(n_neighbors=10).fit(self.points)
        args = self.clf.kneighbors([[x, y]])
        dists, indices = args[0][0], args[1][0]
        radii = self.radii.iloc[indices]['radius']
        col = _time_to_col(time)
        appearances = self.weekly_appearance[col].iloc[indices]

        # values = -appearances
        values = dists / appearances
        candidates = radii[radii > dists].index

        if dists[0] < 0.1 or len(candidates) == 0:
            pt = self.points[indices[0]]
            return pt[0], pt[1]
        else:
            return self.label2xy(values.loc[candidates].argmin())


class ShanghaiLocationData(LocationDataMixin):
    def __init__(self):
        path = data_dir + f'/shanghai/data.txt.gz'
        data = pd.read_csv(path, index_col=0)

        super(ShanghaiLocationData, self).__init__(data)


class RawCdrData(LocationDataMixin):
    def __init__(self, D=60):
        assert D in [40, 60]

        path = data_dir + f'/raw_cdr/D{D}/locations.txt.gz'
        data = pd.read_csv(path, index_col=0)

        self.radii = pd.read_csv(
            data_dir + f'/towers_pospago_xy_radii.txt', index_col=0, header=None, names=['radius'])
        self.weekly_appearance = pd.read_csv(
            data_dir + f'/towers_pospago_xy_week_hour.txt', index_col=0) + 1
        self.clf = None
        super(RawCdrData, self).__init__(data, {})

    def xy2towerxy(self, x, y, time):
        def _time_to_col(time):
            return '%.3d' % (time.weekday() * 100 + time.hour)

        if self.clf is None:
            self.points = self.radii.index.to_series().apply(self.label2xy).values.tolist()
            self.clf = NearestNeighbors(n_neighbors=10).fit(self.points)
        args = self.clf.kneighbors([[x, y]])
        dists, indices = args[0][0], args[1][0]
        radii = self.radii.iloc[indices]['radius']
        col = _time_to_col(time)
        appearances = self.weekly_appearance[col].iloc[indices]

        # values = -appearances
        values = dists / appearances
        candidates = radii[radii > dists].index

        if dists[0] < 0.1 or len(candidates) == 0:
            pt = self.points[indices[0]]
            return pt[0], pt[1]
        else:
            return self.label2xy(values.loc[candidates].argmin())


def _parallel_wrapper(user, run, data, kwargs):
    traj_label = data.trajectory_label(user)
    return run(user, traj_label, data=data, **kwargs)


class DataSource():
    def __init__(self, name, jupyter_notebook=False,**kwargs):
        datasets = {
            'GranData': GranDataLocationData,
            'Shanghai': ShanghaiLocationData
        }
        if name not in datasets:
            raise EnvironmentError('name must be in', list(datasets.keys()))

        dataClass = datasets[name]
        self.data = dataClass(**kwargs)
        self.keys = list(self.data.users)

        self.name = f'{name}'
        self.tqdm = tqdm_notebook if jupyter_notebook else tqdm

    def test_evaluate_func(self, evaluate_class, user_id=0, **kwargs):
        user = self.keys[user_id]
        traj_label = self.data.trajectory_label(user)
        print("Test:", evaluate_class.name)
        return evaluate_class._run(user, traj_label, data=self.data, **kwargs)

    def run(self, evaluate_class, n=None, dump=None, parallel=False, chunksize=3, **kwargs):
        if n is not None:
            n = min(len(self.keys), n)
            keys = self.keys[0:n]
        else:
            keys = self.keys

        temp = []
        if parallel:
            _func = partial(_parallel_wrapper, run=evaluate_class.run,
                            data=self.data, kwargs=kwargs)
            pool = Pool()
            hooks = pool.imap_unordered(_func, keys, chunksize=chunksize)
            pool.close()
            for _res in self.tqdm(hooks, desc='Running', total=len(keys)):
                temp.append(_res)
            pool.join()
        else:
            for key in self.tqdm(keys, desc='Running'):
                traj_label = self.data.trajectory_label(key)
                _res = evaluate_class.run(
                    key, traj_label, data=self.data, **kwargs)
                temp.append(_res)

        results = {}
        for _res in temp:
            for user in _res:
                results[user] = _res[user]

        dt = pd.DataFrame.from_dict(results, orient='index')
        dt = dt.reindex(keys)
        dt = dt.reindex_axis(sorted(dt.columns), axis=1)
        if dump:
            dump_path = f'./result/{self.name}_{evaluate_class.name}.csv.gz'
            print("Dump to", dump_path)
            dt.to_csv(dump_path, compression='gzip')
        return dt

    def runMPI(self, evaluate_class, n=None, ignore=0, dump=None, parallel=False, chunksize=3, **kwargs):
        from mpi4py import MPI
        comm = MPI.COMM_WORLD
        nodes = comm.Get_size()
        rank = comm.Get_rank()
        if rank == 0:
            print("MPI mode:", nodes, "nodes.")
            sys.stdout.flush()

        all_keys = self.keys[ignore:]

        if n is not None:
            n = min(len(all_keys), n)
            keys = all_keys[0:n]
        else:
            n = len(all_keys)
            keys = all_keys

        nodes = min(n, nodes)
        tasks = np.array_split(keys, nodes)
        temp = []
        if rank < nodes:
            node_keys = tasks[rank]
            for key in node_keys:
                traj_label = self.data.trajectory_label(key)
                _res = evaluate_class.run(
                    key, traj_label, data=self.data, **kwargs)
                temp.append(_res)
        # print('Node', rank, 'is done.')
        sys.stdout.flush()
        all_temps = comm.gather(temp, root=0)
        if rank == 0:
            temp = []
            for _r in all_temps:
                temp += _r

            results = {}
            for _res in temp:
                for user in _res:
                    results[user] = _res[user]

            dt = pd.DataFrame.from_dict(results, orient='index')
            dt = dt.reindex(keys)
            dt = dt.reindex_axis(sorted(dt.columns), axis=1)
            if dump:
                dump_path = f'./result/{self.name}_{evaluate_class.name}.csv.gz'
                print("Dump to", dump_path)
                sys.stdout.flush()
                dt.to_csv(dump_path, compression='gzip')
            return dt
        else:
            return pd.DataFrame([])
