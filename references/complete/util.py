import numpy as np
import pandas as pd
from datetime import timedelta

import sys
from multiprocessing import Pool as ProcessPool
from multiprocessing import cpu_count, TimeoutError, current_process
from multiprocessing.dummy import Pool as ThreadPool

from sklearn.externals.joblib import Parallel as _Parallel, delayed as _delayed


def clean(values):
    mean, std = np.mean(values), np.std(values)
    _values = np.array(values)
    good_values = _values[np.abs(_values - mean)<3*std] 
    return good_values

def delayed(*args, **kargs):
    return _delayed(*args, **kargs)


def useProcessPool():
    return ('mpi4py' not in sys.modules) and ('MainProcess' in str(current_process()))


def Pool(*args, **kargs):
    if useProcessPool():
        return ProcessPool(*args, **kargs)
    else:
        return ThreadPool(1)


def Parallel(n_jobs=1):
    if useProcessPool():
        return _Parallel(n_jobs=n_jobs)
    else:
        return _Parallel(n_jobs=1)


def latlon2xy(point, shift90=True):
    import utm
    lat, lon = point
    lat = lat - 90 if shift90 else lat
    res = utm.from_latlon(lat, lon)
    return np.array(res[0:2]) / 1000  # m to km


def ls2p(loc_str, reverse=False):
    # location str to point
    # x,y if reverse y,x
    arr = loc_str.split('|')
    if not reverse:
        # return np.array([float(arr[0]), float(arr[1])])
        return float(arr[0]), float(arr[1])
    else:
        # return np.array([float(arr[1]), float(arr[0])])
        return float(arr[1]), float(arr[0])


def p2ls(point, reverse=False):
    # point to location
    x = '%.6f' % point[0]
    y = '%.6f' % point[1]
    return f'{x}|{y}' if not reverse else f'{y}|{x}'


def dist(u, v):
    """Calculate the great circle distance between two points on the earth (specified in decimal degrees)

    Args:
        u: a point in (latitude, longitude)
        v: a point in (latitude, longitude)

    Returns:
        the distance between u and v in kilometers.
    """

    u = np.array(u)
    reduce = len(u.shape) == 1

    u = u.reshape(-1, 2)
    v = np.array(v).reshape(-1, 2)

    km = np.linalg.norm(u - v, axis=1)
    if reduce:
        return km[0]
    else:
        return km


def rg(pts):
    """Return the radius of gyration of mass of points.

    Radius of Gyration: https://en.wikipedia.org/wiki/Radius_of_gyration


    Args:
        pts: a list of points. Each point is in (latitude, longitude)

    Returns:
        the radius of gyration of points in kilometers

    """
    pts = np.array(pts).reshape(-1, 2)
    # remove nan rows
    pts = pts[~np.isnan(pts).any(axis=1)]
    # get unique
    # pts = np.unique(pts, axis=0)
    # p = np.random.rand(pts.shape[1])
    # _, index = np.unique(pts.dot(p), return_index=True)
    # pts = pts[index]
    r_mean = np.ones(pts.shape) * np.mean(pts, axis=0)
    rg2 = np.mean(dist(pts, r_mean) ** 2)
    return np.sqrt(rg2)


def _generate_random_loss(traj, loss_rate=0.8, random_state=19881102):
    num_na = traj[traj.isnull()].size
    num_remove = int(traj.size * loss_rate) - num_na
    _traj = traj.copy()
    loss_index = traj[traj.notnull()].sample(
        n=num_remove, random_state=random_state).index
    _traj[loss_index] = np.nan
    return _traj, loss_index


probs = [0.010174286230840813, 0.005067285347563313, 0.00303594587834457, 0.0021660942842690457, 0.0019948618064215876,
         0.003191937818835303, 0.007699813609058351, 0.018843798895290895, 0.03575405394379776, 0.05386215118270531,
         0.06454762994320543, 0.06919142048976554,
         0.07194643523024723, 0.07527608346094941, 0.07618969022266418, 0.07045591779296373, 0.06618419086748954,
         0.06496214172242343, 0.06582666327954323, 0.06412169428415708, 0.0611228143414913, 0.05160262825332042,
         0.03668937698870332, 0.020093084125949207]
cdfs = [sum(probs[0:i + 1]) for i in range(len(probs) - 1)] + [1.]


def _generate_callpattern_loss(traj, loss_rate=0.8, random_state=19881102):
    rand = np.random.RandomState(random_state)

    def randhour_by_calldist():
        mark = rand.random_sample()
        for i in range(len(cdfs)):
            if cdfs[i] > mark:
                return i
        raise Exception('something is wrong')

    num_remain = int(np.ceil(traj.size * (1 - loss_rate)))
    ids_remain = []
    good_index = pd.DatetimeIndex(traj[traj.notnull()].index).copy()
    while len(ids_remain) < num_remain:
        hour = randhour_by_calldist()
        candidates = good_index[good_index.hour == hour].tolist()
        if len(candidates) == 0:
            continue
        select = rand.choice(candidates, 1)[0]
        good_index = good_index.drop(select)
        ids_remain.append(select)

    _traj = traj.copy()
    _index = pd.DatetimeIndex(traj[traj.notnull()].index)
    loss_index = traj[traj.notnull()].index[~_index.isin(ids_remain)]
    _traj[loss_index] = np.nan
    return _traj, loss_index


def loss(traj_label, frac, pattern='random', random_state=None):
    if pattern == 'random':
        return _generate_random_loss(traj_label, frac, random_state)
    elif pattern == 'call':
        return _generate_callpattern_loss(traj_label, frac, random_state)
    else:
        raise EnvironmentError('pattern must be "random" or "call"')


def parse_timedelta(time_str):
    """
    Helper function to convert human readable time offsets to Python timedelta objects.
    This is simply to enable readability in the parameter specification.

    :param time_str: A time offset string in the form 'h:mm:ss'
    :type time_str: str
    """
    split = time_str.split(':')
    return timedelta(hours=int(split[0]), minutes=int(split[1]), seconds=int(split[2]))


def expand(traj_label, min_days=15):
    days = np.unique(traj_label.index.date).size
    weeks = (days - 0.1) // 7 + 1
    if days < min_days:
        index = traj_label.index
        traj_label_expanded = []
        for i in range(int((min_days - 0.1) // days + 1)):
            _traj_label = traj_label.copy()
            _traj_label.index = index
            traj_label_expanded.append(_traj_label)
            index += parse_timedelta(f'{7*24}:00:00')

        traj_label_expanded = pd.concat(traj_label_expanded)
        return traj_label_expanded.iloc[0:(min_days * 24)]
    else:
        return traj_label.iloc[0:(min_days * 24)]
