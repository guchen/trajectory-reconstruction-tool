import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score

from complete.method import matrix_completion, tensor_completion, interpolation
from complete.util import loss, dist, rg, Parallel, delayed, clean


class BaseEvaluation():
    def __init__(self):
        self.name = 'BaseEvaluation'

    def method(self, traj_label, data, **kwargs):
        raise NotImplementedError

    def run(self, user, traj_label, data, **kwargs):
        try:
            result = self._run(user, traj_label, data, **kwargs)
            return result
        except Exception as e:
            print('Error in user:', user, 'info:', str(e))
            return {}

    def _run(self, user, traj_label_orig, data, progress=False, **kwargs):
        result = {}
        # expand
        traj_label = traj_label_orig
        traj_xy = data.traj2xys(traj_label)
        # radius of gyration
        result[f'rg'] = rg(traj_xy.values)
        for completion in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 30, 40, 50, 60]:
            loss_rate = 1.0 - completion / 100.
            mae, mrg = [], []
            i = 0
            for traj_loss in data.loss(traj_label, C=completion):
                i += 1
                if progress:
                    print(f'C{completion}', i)
                loss_index = traj_loss[traj_loss.isnull()].index
                # complete
                traj_est_xy = self.method(traj_loss, data=data, **kwargs)
                # mean radius of gyration
                mrg.append(rg(traj_est_xy.values))
                # mean distance error
                X = traj_xy.loc[loss_index].values
                X_hat = traj_est_xy.loc[loss_index].values
                mae.append(dist(X, X_hat).mean())
            
            result[f'mae-C{completion}-{self.name}'] = np.mean(clean(mae))
            result[f'rg-C{completion}-{self.name}'] = np.mean(clean(mrg))
        return {user: result}


class FillMean(BaseEvaluation):
    def __init__(self):
        self.name = 'fillmean'

    def method(self, traj_label, data, **kwargs):
        traj_xy = data.traj2xys(traj_label)
        loss = traj_xy['x'].isnull()
        traj_xy['x'][loss] = traj_xy['x'][~loss].mean()
        traj_xy['y'][loss] = traj_xy['y'][~loss].mean()
        return traj_xy


class LastSeen(BaseEvaluation):
    def __init__(self):
        self.name = 'lastseen'

    def method(self, traj_label, data, **kwargs):
        traj_label_est = traj_label.fillna(
            method='ffill').fillna(method='bfill')
        traj_xy_est = data.traj2xys(traj_label_est)
        return traj_xy_est


class InterP(BaseEvaluation):
    def __init__(self):
        self.name = 'interP'

    def method(self, traj_label, data, **kwargs):
        return interpolation(traj_label, data, **kwargs)


class MC(BaseEvaluation):  # matrix completion
    def __init__(self, spothome=False):
        self.name = 'mc-spothome' if spothome else 'mc'
        self.spothome = spothome

    def method(self, traj_label, data, **kwargs):
        if 'spothome' in kwargs:
            del kwargs['spothome']
        return matrix_completion(traj_label, data, spothome=self.spothome, **kwargs)


class TC(BaseEvaluation):  # matrix completion
    def __init__(self, spothome=False):
        self.name = 'tc-spothome' if spothome else 'tc'
        self.spothome = spothome

    def method(self, traj_label, data, **kwargs):
        if 'spothome' in kwargs:
            del kwargs['spothome']
        # # #
        return tensor_completion(traj_label, data, spothome=self.spothome, **kwargs)
