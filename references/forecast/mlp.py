from sklearn.model_selection import ParameterGrid
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler

from forecast.baseline import ts_to_baseline_XY, ts_l_to_locs, ts_v_to_mags
from forecast.util import *


def _ts_index_to_time_features(ts_index):
    weeks = ts_index.to_series().map(lambda dt: dt.weekday())
    weeks = weeks.values.reshape(-1, 1)

    hours = np.array(ts_index.hour).reshape(-1, 1)

    time_diffs = ts_index.to_series() - ts_index[0]

    hour_diffs = time_diffs.map(
        lambda df: df.total_seconds() / 3600.0).values.reshape(-1, 1)
    day_diffs = time_diffs.map(
        lambda df: df.total_seconds() / 3600.0 / 24).values.reshape(-1, 1)

    return hours, weeks, day_diffs, hour_diffs


def _TSD_to_XY_align_time(TSD, order, time_begin=-2):
    n_samples, width = TSD.shape
    X = []
    for i in range(order, n_samples):
        X.append(TSD[i - order:i + 1])

    X = np.array(X).reshape(len(X), -1)
    for i in range(1, order + 1):
        X[:, time_begin - width * i] += -X[:, time_begin]  # hour
        X[:, time_begin - 1 - width * i] += -X[:, time_begin - 1]  # day
    X[:, time_begin] = 0
    X[:, time_begin - 1] = 0

    Y = X[:, (time_begin + 1):]
    X = X[:, 0:(time_begin + 1)]
    return X, Y


def ts_v_to_mlp_XY(ts_v, order=2, locs=None, top_loc=15):
    sep = np.unique(np.floor(ts_v.values)).mean() + 0.7
    diffs = list(map(lambda v: 1 if v > sep else -1, ts_v.values.tolist()))
    diffs = np.array(diffs).reshape(-1, 1)

    ts_v.index = pd.DatetimeIndex(ts_v.index)
    time_features = _ts_index_to_time_features(ts_v.index)
    vols = ts_v.values.reshape(-1, 1)
    TSD = np.hstack((diffs,) + time_features + (vols,))
    if locs is not None:
        keys = locs.value_counts().keys()
        locs = locs.apply(keys.get_loc)
        if top_loc is not None:
            locs[locs >= top_loc] = top_loc
        locs = locs.values.reshape(-1, 1)
        TSD = np.hstack((locs, diffs) + time_features + (vols,))

    return _TSD_to_XY_align_time(TSD, order)


def ts_l_to_mlp_XY(ts_l, order=2, top_loc=15):
    time_features = _ts_index_to_time_features(ts_l.index)

    locs = ts_l.copy()
    keys = locs.value_counts().keys()
    locs = locs.apply(keys.get_loc)
    if top_loc is not None:
        locs[locs >= top_loc] = top_loc
    locs = locs.values.reshape(-1, 1)
    locs_onehot = OneHotEncoder(sparse=False).fit(np.arange(0, locs.max() + 1).reshape(-1, 1)).transform(locs)
    TSD = np.concatenate(time_features + (locs_onehot,), axis=1)
    X, Y = _TSD_to_XY_align_time(TSD, order, time_begin=(-locs_onehot.shape[1] - 1))
    Y = locs[-(Y.shape[0]):]
    return X, Y


def ts_vl_to_mlp_XY(ts_v, ts_l, order=2, top_loc=15):
    time_features = _ts_index_to_time_features(ts_l.index)
    # vols
    sep = np.unique(np.floor(ts_v.values)).mean() + 0.7
    diffs = list(map(lambda v: 1 if v > sep else -1, ts_v.values.tolist()))
    diffs = np.array(diffs).reshape(-1, 1)
    vols = ts_v.values.reshape(-1, 1)
    # locs
    locs = ts_l.copy()
    keys = locs.value_counts().keys()
    locs = locs.apply(keys.get_loc)
    if top_loc is not None:
        locs[locs >= top_loc] = top_loc
    locs = locs.values.reshape(-1, 1)
    locs_onehot = OneHotEncoder(sparse=False).fit(np.arange(0, locs.max() + 1).reshape(-1, 1)).transform(locs)

    # TSD
    TSD = np.concatenate((diffs,) + time_features + (locs_onehot, vols), axis=1)
    time_begin = -1 - locs_onehot.shape[1] - vols.shape[1]
    X, Y = _TSD_to_XY_align_time(TSD, order, time_begin)
    Yv = Y[:, -(vols.shape[1]):]
    Yl = Y[:, 0: -(vols.shape[1])]
    Yl = locs[-(Yl.shape[0]):]
    return X, Yv, Yl


def _mlp_sequential_predictor(X, Y, n_predict, n_update, scale, verbose=False, **kwargs):
    if scale:
        X = StandardScaler().fit_transform(X)

    Y_test = Y[-n_predict:]

    encoder = OneHotEncoder(sparse=False).fit(
        np.arange(0, Y.max() + 1).reshape(-1, 1))
    Y = encoder.transform(Y.reshape(-1, 1))
    if Y.shape[1] == 1:  # means only one state at all time
        # 100% prediction
        return Y_test, Y_test
    # hidden_size  = max(5,int(2*(Y.shape[1]+X.shape[1])/3))
    # hidden_size = min(hidden_size, 50)
    hidden_size = 20
    model = MLPClassifier(hidden_layer_sizes=(20, 20),  # (256,4) (20,20)
                          activation='relu', batch_size=5 * 24, random_state=19881102,
                          learning_rate_init=0.01, max_iter=400,
                          warm_start=True, verbose=verbose, **kwargs).fit(X[0:-n_predict], Y[0:-n_predict])
    Y_predict = []
    n_total = X.shape[0]
    n_start = n_total - n_predict
    while n_start < n_total:
        n_end = min(n_total, n_start + n_update)
        _Y_predict = model.predict(X[n_start:n_end]).argmax(
            axis=1).astype(float).tolist()
        Y_predict += _Y_predict
        model = model.fit(X[0:n_end], Y[0:n_end])
        n_start += n_update

    return Y_predict, Y_test


def mlp_mags_sequential_predictor(mags_seq, n_predict, n_update, order, scale=True, simple=False, verbose=False,
                                  **kwargs):
    if simple:
        X, Y = ts_to_baseline_XY(mags_seq, order=order)
        X, Y = np.array(X), np.array(Y)
    else:
        X, Y = ts_v_to_mlp_XY(mags_seq, order=order)

    Y_predict, Y_test = _mlp_sequential_predictor(
        X, Y, n_predict, n_update, scale, verbose, **kwargs)
    return Y_predict


def mlp_locs_sequential_predictor(locs_seq, n_predict, n_update, order, scale=True, simple=False, verbose=False,
                                  **kwargs):
    if simple:
        X, Y = ts_to_baseline_XY(locs_seq, order=order)
        X, Y = np.array(X).astype('float'), np.array(Y).astype('float')
    else:
        X, Y = ts_l_to_mlp_XY(locs_seq, order=order)

    Y_predict, Y_test = _mlp_sequential_predictor(
        X, Y, n_predict, n_update, scale, verbose, **kwargs)
    return Y_predict, Y_test


def compute_mlp_volume(phone, ts_v, results, n_predict, n_update, grid=False, verbose=False):
    mags = vols_to_mags(ts_v)
    vols_test = ts_v.values[-n_predict:]

    for simple in [True, False]:
        params = {
            'simple': [simple],
            'scale': [True],
            'alpha': [1, 0.1, 0.01, 0.001, 0.0001, 0.00001] if grid else [0.0001]
        }
        for order in [1, 2, 3, 4, 5, 6, 7, 8]:
            prefix = f'mlp-mags-{order}'
            if simple:
                prefix = 'simple-' + prefix
            n_jobs = min(cpu_count(), len(params['alpha']))
            i = 0
            tasks = []
            pool = Pool(n_jobs)
            for kwargs in ParameterGrid(params):
                results.loc[phone, f'{prefix}-params-{i}'] = str(kwargs)

                kwargs = dict(n_predict=n_predict, n_update=n_update,
                              order=order, verbose=verbose, **kwargs)
                hook = pool.apply_async(
                    mlp_mags_sequential_predictor, (mags,), kwargs)
                tasks.append(hook)
                i += 1
            pool.close()

            accu_max = -100.
            for i in range(len(tasks)):
                mags_predict = tasks[i].get()
                compute_rmse(phone, vols_test, results, mags_predict,
                             f'{prefix}-params-{i}', 'mags')
                accu_cur = results.loc[phone, f'accu-{prefix}-params-{i}']
                if accu_cur > accu_max:
                    accu_max = accu_cur
                    results.loc[phone, f'best-param-{order}'] = int(i)
                    compute_rmse(phone, vols_test, results, mags_predict,
                                 f'{prefix}', 'mags')
            pool.join()
            del pool


def mlp_mags_VwithL_sequential_predictor(mags_seq, locs_seq, n_predict, n_update, order, scale=True, verbose=False,
                                         **kwargs):
    X, Y = ts_v_to_mlp_XY(mags_seq, order=order, locs=locs_seq)
    Y_predict, Y_test = _mlp_sequential_predictor(
        X, Y, n_predict, n_update, scale, verbose, **kwargs)
    return Y_predict


def mlp_mags_VL_sequential_predictor(mags_seq, locs_seq, n_predict, n_update, order, scale=True, simple=False,
                                     verbose=False,
                                     **kwargs):
    if simple:
        Xv, Yv = ts_to_baseline_XY(mags_seq, order=order)
        Xl, Yl = ts_to_baseline_XY(locs_seq, order=order)
        X = np.hstack((Xv, Xl))
        Yv = np.array(Yv)
        Yl = np.array(Yl)
    else:
        X, Yv, Yl = ts_vl_to_mlp_XY(mags_seq, locs_seq, order=order)

    Yv_test, Yl_test = Yv[-n_predict:], Yl[-n_predict:]

    if scale:
        scaler = StandardScaler().fit(X)
        X = scaler.transform(X)

    # Note that both Yv, Yl are 0,1,2,3,4,....
    encoder_v = OneHotEncoder(sparse=False).fit(
        np.arange(0, Yv.max() + 1).reshape(-1, 1))
    encoder_l = OneHotEncoder(sparse=False).fit(
        np.arange(0, Yl.max() + 1).reshape(-1, 1))

    Yv = encoder_v.transform(Yv.reshape(-1, 1))
    Yl = encoder_l.transform(Yl.reshape(-1, 1))
    Y = np.concatenate((Yv, Yl), axis=1)

    model = MLPClassifier(hidden_layer_sizes=(20, 20),
                          activation='relu', batch_size=5 * 24, random_state=19881102,
                          learning_rate_init=0.01, warm_start=True,
                          max_iter=400, **kwargs).fit(X[0:-n_predict], Y[0:-n_predict])

    Yv_predict, Yl_predict = [], []
    n_total = X.shape[0]
    n_start = n_total - n_predict
    while n_start < n_total:
        n_end = min(n_total, n_start + n_update)
        _probas = model.predict_proba(X[n_start:n_end])
        Yv_predict.append(_probas[:, 0:Yv.shape[1]])
        Yl_predict.append(_probas[:, Yv.shape[1]:])
        model = model.fit(X[0:n_end], Y[0:n_end])
        n_start += n_update

    Yv_predict = np.concatenate(Yv_predict).argmax(axis=1)
    Yl_predict = np.concatenate(Yl_predict).argmax(axis=1)

    return Yv_predict, Yl_predict, Yv_test, Yl_test


def compute_mlp_volume_with_location(phone, ts_v, ts_l, results, n_predict, n_update, order=4, grid=True,
                                     verbose=False):
    mags = vols_to_mags(ts_v)
    vols_test = ts_v.values[-n_predict:]
    locs = ts_l.copy()

    params = {
        'scale': [True],
        'alpha': [1, 0.1, 0.01, 0.001, 0.0001, 0.00001] if grid else [0.0001]
    }

    n_jobs = min(cpu_count(), len(params['alpha']))

    i = 0
    tasks = []
    pool = Pool(n_jobs)
    for kwargs in ParameterGrid(params):
        results.loc[phone, 'params-%d' % i] = str(kwargs)
        kwargs = dict(n_predict=n_predict, n_update=n_update,
                      order=order, verbose=verbose, **kwargs)
        hook = pool.apply_async(
            mlp_mags_VwithL_sequential_predictor, (mags, locs), kwargs)
        tasks.append(hook)
        i += 1
    pool.close()

    accu_max = -100.
    for i in range(len(tasks)):
        mags_predict = tasks[i].get()
        compute_rmse(phone, vols_test, results, mags_predict,
                     'mlp-V|L-mags-params-%d' % i, 'mags')
        accu_cur = results.loc[phone, 'accu-mlp-V|L-mags-params-%d' % i]
        if accu_cur > accu_max:
            accu_max = accu_cur
            results.loc[phone, 'best-param'] = int(i)
            compute_rmse(phone, vols_test, results, mags_predict,
                         'mlp-V|L-mags', 'mags')
    pool.join()
    del pool


def compute_mlp_location(phone, ts_v, ts_l, results, n_predict, n_update, verbose=False, grid=False):
    locs = ts_l_to_locs(ts_l, top_loc=15)

    for simple in [True, False]:
        for order in [3, 4, 5, 6, 7, 8]:
            prefix = f'mlp-locs-{order}'
            if simple:
                prefix = 'simple-' + prefix

            params = {
                'simple': [simple],
                'scale': [True],
                'alpha': [1, 0.1, 0.01, 0.001, 0.0001, 0.00001] if grid else [0.0001]
            }

            n_jobs = min(cpu_count(), len(params['alpha']))

            i = 0
            tasks = []
            pool = Pool(n_jobs)
            for kwargs in ParameterGrid(params):
                results.loc[phone, f'{prefix}-params-{i}'] = str(kwargs)

                kwargs = dict(n_predict=n_predict, n_update=n_update,
                              order=order, verbose=verbose, **kwargs)
                hook = pool.apply_async(
                    mlp_locs_sequential_predictor, (locs,), kwargs)
                tasks.append(hook)
                i += 1
            pool.close()

            accu_max = -100.
            for i in range(len(tasks)):
                locs_predict, locs_test = tasks[i].get()
                results.loc[phone, f'accu-mlp-locs-params-{i}'] = accuracy_score(locs_predict, locs_test)

                accu_cur = results.loc[phone, f'accu-mlp-locs-params-{i}']
                if accu_cur > accu_max:
                    accu_max = accu_cur
                    results.loc[phone, f'best-param-{order}'] = int(i)
                    results.loc[phone, f'accu-{prefix}'] = accu_cur

            pool.join()
            del pool


def compute_mlp_volume_location(phone, ts_v, ts_l, results, n_predict, n_update, grid=False, verbose=False):
    mags = vols_to_mags(ts_v)
    locs = ts_l_to_locs(ts_l, top_loc=15)

    for simple in [True, False]:
        for order in [2, 3, 4, 5, 6]:
            prefix = f'mlp-VL-{order}'
            if simple:
                prefix = 'simple-' + prefix
            # print(prefix)

            ## sep
            if order == 4:
                mags_predict = mlp_mags_sequential_predictor(mags, n_predict, n_update, simple=simple, scale=True,
                                                             alpha=0.01, order=order, verbose=verbose)
                locs_predict, locs_test = mlp_locs_sequential_predictor(locs, n_predict, n_update, simple=simple,
                                                                        scale=True, alpha=0.01, order=order,
                                                                        verbose=verbose)
                mags_test = mags[-n_predict:].tolist()
                accu = 0.
                total = 0.
                for j in range(len(mags_predict)):
                    total += 1
                    test_mag = (mags_predict[j] == mags_test[j])
                    test_loc = (locs_predict[j] == locs_test[j])
                    if test_mag and test_loc:
                        accu += 1
                results.loc[phone, f'accu-{prefix}-sep'] = accu / total
            ## sep end

            params = {
                'scale': [True],
                'simple': [simple],
                'alpha': [1, 0.1, 0.01, 0.001, 0.0001, 0.00001] if grid else [0.0001]
            }

            parameters = list(ParameterGrid(params))
            n_jobs = max(int(cpu_count() / 2), 1)

            i = 0
            tasks = []
            pool = Pool(n_jobs)
            for kwargs in parameters:
                results.loc[phone, f'{prefix}-params-{i}'] = str(kwargs)

                kwargs = dict(n_predict=n_predict, n_update=n_update,
                              order=order, verbose=verbose, **kwargs)
                hook = pool.apply_async(
                    mlp_mags_VL_sequential_predictor, (mags, locs), kwargs)
                tasks.append(hook)
                i += 1
            pool.close()

            accu_max = -100.
            for i in range(len(tasks)):
                Yv_predict, Yl_predict, Yv_test, Yl_test = tasks[i].get()

                total = accu_v = accu_l = accu_both = 0
                for j in range(len(Yv_predict)):
                    total += 1
                    if Yv_predict[j] == Yv_test[j]:
                        accu_v += 1
                    if Yl_predict[j] == Yl_test[j]:
                        accu_l += 1
                    if (Yv_predict[j] == Yv_test[j]) and (Yl_predict[j] == Yl_test[j]):
                        accu_both += 1
                accu_v /= total
                accu_l /= total
                accu_both /= total

                results.loc[phone, f'accu-{prefix}-mags-{i}'] = accu_v
                results.loc[phone, f'accu-{prefix}-locs-{i}'] = accu_l

                accu_cur = accu_v + accu_l
                if accu_cur > accu_max:
                    accu_max = accu_cur
                    results.loc[phone, f'{prefix}-best-param'] = int(i)
                    results.loc[phone, f'accu-{prefix}-mags'] = accu_v
                    results.loc[phone, f'accu-{prefix}-locs'] = accu_l
                    results.loc[phone, f'accu-{prefix}-both'] = accu_both
