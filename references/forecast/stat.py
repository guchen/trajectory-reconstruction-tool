import numpy as np

def dist(u, v):
    """Calculate the great circle distance between two points on the earth (specified in decimal degrees)

    Args:
        u: a point in (latitude, longitude)
        v: a point in (latitude, longitude)

    Returns:
        the distance between u and v in kilometers.
    """

    u = np.array(u)
    reduce = len(u.shape) == 1

    u = u.reshape(-1, 2)
    v = np.array(v).reshape(-1, 2)

    # RADIUS_EARTH = 6367  # kilometers
    RADIUS_EARTH = 6371.009

    # convert decimal degrees to radians
    lat1 = np.radians(u[:, 0])
    lon1 = np.radians(u[:, 1])
    lat2 = np.radians(v[:, 0])
    lon2 = np.radians(v[:, 1])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat / 2) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2) ** 2
    c = 2 * np.arcsin(np.sqrt(a))
    km = RADIUS_EARTH * c

    # no need to use numexpr, still slow
    # for import numexpr as ne
    # a = ne.evaluate('sin((lat2-lat1)/2)**2 + cos(lat1) *cos(lat2) * sin((lon2-lon1)/2)**2')
    # km = ne.evaluate('RADIUS_EARTH * 2 * arcsin(sqrt(a))')

    if reduce:
        return km[0]
    else:
        return km


def rg(pts):
    """Return the radius of gyration of mass of points.

    Radius of Gyration: https://en.wikipedia.org/wiki/Radius_of_gyration


    Args:
        pts: a list of points. Each point is in (latitude, longitude)

    Returns:
        the radius of gyration of points in kilometers

    """
    pts = np.array(pts).reshape(-1, 2)
    # remove nan rows
    pts = pts[~np.isnan(pts).any(axis=1)]
    # get unique
    # pts = np.unique(pts, axis=0)
    # p = np.random.rand(pts.shape[1])
    # _, index = np.unique(pts.dot(p), return_index=True)
    # pts = pts[index]
    r_mean = np.ones(pts.shape) * np.mean(pts, axis=0)
    rg2 = np.mean(dist(pts, r_mean) ** 2)
    return np.sqrt(rg2)

def _loc2point(loc):
    _arr = loc.split('|')
    return float(_arr[0]), float(_arr[1])

def compute_stats(phone, ts_v, ts_l, results):
    # need frequency to be '1H'
    results.loc[phone, 'n_days'] = len(np.unique(ts_v.index.date))
    
    stat = ts_v.groupby(ts_v.index.date).sum()
    results.loc[phone, 'vol_daily_max'] = stat.max()
    results.loc[phone, 'vol_daily_mean'] = stat.mean()
    results.loc[phone, 'vol_daily_min'] = stat.min()
    
    results.loc[phone, 'vol_total'] = ts_v.sum()
    
    results.loc[phone, 'n_locs_total'] = len(ts_l.unique())
    
    stat = ts_l.groupby(ts_l.index.date).apply(lambda locs: len(np.unique(locs)))
    results.loc[phone, 'n_locs_daily_max'] = stat.max()
    results.loc[phone, 'n_locs_daily_mean'] = stat.mean()
    results.loc[phone, 'n_locs_daily_min'] = stat.min()
    
    _ts = ts_v[ts_v>0]
    stat = _ts.groupby(_ts.index.date).count()
    results.loc[phone, 'active_hours_daily_max'] = stat.max()
    results.loc[phone, 'active_hours_daily_mean'] = stat.mean()
    results.loc[phone, 'active_hours_daily_min'] = stat.min()

    total = len(ts_l.index)
    counts = ts_l.value_counts()
    for k in range(1, 31):
        n_top_k = len(ts_l[ts_l.isin(counts.keys()[0:k])].index)
        results.loc[phone, f'ratio_locs_top_{k}'] = n_top_k / total

    #rg
    points = ts_l.apply(_loc2point).tolist()
    results.loc[phone,'rg'] = rg(points)