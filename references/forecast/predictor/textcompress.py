import numpy as np

from forecast.predictor.base import _Seq2SeqPredictorBase


class _TextPredictorBase(_Seq2SeqPredictorBase):
    def fit_state(self, state):
        raise NotImplementedError

    def proba_next(self, prefix=None):
        counts = self._get_counts(prefix)
        return counts

    def fit_predict_proba(self, time_series, n_ratio_predict=0.9):
        predict_start = None
        if n_ratio_predict < 1:
            predict_start = int(len(time_series) * n_ratio_predict)
        elif n_ratio_predict >= 1:
            predict_start = len(time_series) - n_ratio_predict

        self.fit(time_series[0:predict_start])

        Y_proba = []
        for i in range(predict_start, len(time_series)):
            prefix = time_series[0:i]
            # predict
            proba = self.proba_next(prefix=prefix)
            Y_proba.append(proba)
            # train
            state = time_series[i]
            self.fit_state(state)
        return Y_proba

    def fit(self, time_series):
        raise NotImplementedError

    def _get_counts(self, prefix=None):
        raise NotImplementedError


class SPMPredictor(_TextPredictorBase):
    def __init__(self, alpha):
        super(SPMPredictor, self).__init__()
        self.alpha = alpha
        if alpha <= 0. or alpha > 1.:
            raise EnvironmentError('Alpha need to be in (0,1].')

    def longest_suffix(self, time_series):
        matched_suffix = []
        i = 1
        while 1:
            suffix = time_series[-i:]
            goto_next = False
            for j in range(len(time_series) - len(suffix)):
                if time_series[j] == suffix[0] and time_series[j:j + len(suffix)] == suffix:
                    matched_suffix = suffix
                    goto_next = True
                    break
            if goto_next:
                i += 1
            else:
                break
        return matched_suffix

    def fit_state(self, state):
        # nothing to do
        return self

    def fit(self, time_series):
        # nothing to do
        return self

    def _get_counts(self, prefix=None):
        if prefix is None:
            prefix = []
        if len(prefix) == 0:
            raise Exception('Need history!')

        counts = {}
        suffix = self.longest_suffix(prefix)
        if len(suffix) == 0:
            counts[prefix[-1]] = 1
            return counts
        else:
            N = int(np.ceil(self.alpha * len(suffix)))
            matched = suffix[-N:]
            for i in range(len(prefix) - N):
                if prefix[i] == matched[0] and prefix[i:i + N] == matched:
                    candidate = prefix[i + N]
                    if candidate in counts:
                        counts[candidate] += 1
                    else:
                        counts[candidate] = 1

            return counts


class ALZPredictor(_TextPredictorBase):
    """
    Active LeZi Predictor
    """

    def __init__(self):
        super(ALZPredictor, self).__init__()
        self.ALZ_tree = {'state': '', 'freq': 0, 'children': {}}
        self.dictionary = {}
        self.phase = []
        self.window = []
        self.time_series = []
        self.Max_LZ_length = 0

    def _update_ALZ_tree(self):
        window = self.window
        ALZ_tree = self.ALZ_tree
        for j in range(len(window)):
            key = tuple(window[j:])
            # locate places
            cur = ALZ_tree
            for i in range(len(key)):
                state = key[i]
                if state not in cur['children']:
                    cur['children'][state] = {
                        'state': state, 'freq': 0, 'children': {}}
                cur = cur['children'][state]
            # add frequency
            cur['freq'] += 1

        ALZ_tree['freq'] = sum([ALZ_tree['children'][key]['freq']
                                for key in ALZ_tree['children']])
        self.ALZ_tree = ALZ_tree
        return self

    def fit_state(self, state):
        v = [state]
        key = tuple(self.phase + v)
        if key in self.dictionary:
            self.phase = self.phase + v
        else:
            self.dictionary[key] = 1
            if self.Max_LZ_length < len(self.phase + v):
                self.Max_LZ_length = len(self.phase + v)
            self.phase = []

        self.window += v
        if len(self.window) > self.Max_LZ_length:
            # remove window[0]
            self.window = self.window[1:]

        self._update_ALZ_tree()
        self.time_series += v

    def fit(self, time_series):
        for i in range(len(time_series)):
            self.fit_state(time_series[i])
        return self

    def _get_counts(self, prefix=None):
        probabilities = {}
        fall_back_prob = 1.
        window = self.window

        for i in range(1, len(window)):
            suffix = window[i:]
            # find suffix root
            root = self.ALZ_tree
            # no need to check if has_key??
            for j in range(len(suffix)):
                state = suffix[j]
                root = root['children'][state]

            for key in root['children']:
                state = root['children'][key]['state']
                state_freq = root['children'][key]['freq']
                if state not in probabilities:
                    probabilities[state] = 0.
                probabilities[state] += fall_back_prob * \
                                        1.0 * state_freq / root['freq']

            sum_children_freqs = sum(
                [root['children'][key]['freq'] for key in root['children']])
            fall_back_prob *= 1.0 * \
                              (root['freq'] - sum_children_freqs) / root['freq']

        # root
        root = self.ALZ_tree
        for key in root['children']:
            state = root['children'][key]['state']
            state_freq = root['children'][key]['freq']
            if state not in probabilities:
                probabilities[state] = 0.
            probabilities[state] += fall_back_prob * \
                                    1.0 * state_freq / root['freq']

        return probabilities
