import pandas as pd
from qbmn.base import Analyzer
import numpy as np

np.random.seed(19881102)
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import HuberRegressor
from sklearn.svm import SVC, NuSVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
from tqdm import tqdm
from itertools import izip
from copy import deepcopy
import operator


# idle == 0

class Predictor(object):
    def fit(self, time_series):
        raise NotImplementedError

    def fit_predict(self, time_series, ratio_fit=0.9):
        raise NotImplementedError

    def predict_next(self, prefix=None):
        # type: (object) -> float
        raise NotImplementedError



class IntStatePredictor(Predictor):
    def __init__(self):
        super(IntStatePredictor, self).__init__()
        self.classes_ = None
        self.encoder = {}

    def bootstrap(self):
        if self.classes_ is None or len(self.classes_) == 0:
            raise EnvironmentError('Not classes!')

            # if self.encoder is None:
            #     print self.classes_
            #     self.encoder = LabelEncoder().fit(self.classes_)

    def unique(self, time_series):
        for item in time_series:
            if not self.encoder.has_key(item):
                self.encoder[item] = len(self.encoder)

        return self.encoder.keys()

    def _2int(self, time_series):
        # if hasattr(time_series, '__iter__') and not isinstance(time_series, tuple):
        #     return self.encoder.transform(time_series)
        # else:
        #     return self.encoder.transform([time_series])[0]
        if hasattr(time_series, '__iter__') and not isinstance(time_series, tuple):
            return [self.encoder[item] for item in time_series]
            # return self.encoder.transform(time_series)
        else:
            return self._2int([time_series])[0]
            # return self.encoder.transform([time_series])[0]

    def _2class(self, int_series):

        # if hasattr(int_series, '__iter__') and not isinstance(int_series, tuple):
        #     return self.encoder.inverse_transform(int_series)
        # else:
        #     return self.encoder.inverse_transform([int_series])[0]

        reverse_encoder = dict(zip(self.encoder.values(), self.encoder.keys()))

        if hasattr(int_series, '__iter__') and not isinstance(int_series, tuple):
            return [reverse_encoder[item] for item in int_series]
        else:
            return self._2class([int_series])[0]

    def fit(self, time_series):
        if self.classes_ is None:
            self.classes_ = np.unique(time_series)

        self.bootstrap()

        int_series = self._2int(time_series)

        return self._fit_int(int_series)

    def _fit_int(self, int_series):
        raise NotImplementedError

    def predict_next(self, prefix=None):
        if prefix is None:
            prefix = []
        if len(prefix) != self.order:
            raise Exception(
                'the size of prefix must be equal to the order. Received: {}, {}'.format(prefix, self.order))

        prefix_int = self._2int(prefix)
        next_state_int = self._predict_next_int(prefix_int)

        return self._2class(next_state_int)

    def _predict_next_int(self, prefix_int=None):
        raise NotImplementedError

    def fit_predict(self, time_series, ratio_fit=0.9):
        if self.classes_ is None:
            # self.classes_ = np.unique(time_series)
            self.classes_ = self.unique(time_series)

        self.bootstrap()

        int_series = self._2int(time_series)
        score, predicted, real = self._fit_predict_int(int_series, ratio_fit=ratio_fit)
        return score, self._2class(predicted), self._2class(real)

    def _fit_predict_int(self, int_series, ratio_fit=0.9):
        raise NotImplementedError


def keymax(stats):
    return max(stats.iteritems(), key=operator.itemgetter(1))[0]


class MarkovPredictor1(Predictor):
    def __init__(self, order, fallback_count=2):
        self.order = order
        self.fallback_count = fallback_count
        self.history = {}

    def _add_transition(self, prefix, next):
        if self.order > 0:
            key = tuple(prefix)
            if not self.history.has_key(key):
                self.history[key] = {}

            if self.history[key].has_key(next):
                self.history[key][next] += 1
                return self
            else:
                self.history[key][next] = 1
                return self
        else:
            if self.history.has_key(next):
                self.history[next] += 1
                return self
            else:
                self.history[next] = 1
                return self

    def fit(self, time_series):
        order = self.order
        for i in range(order, len(time_series)):
            next = time_series[i]
            prefix = time_series[i - order:i]
            self._add_transition(prefix, next)

        return self

    def _get_counts(self, prefix=None, _history=None, _order=None, condition_func=None):
        # predict
        order = self.order if _order is None else _order
        history = self.history if _history is None else _history

        def check_whenever_has_condition(counts):
            if condition_func is None:
                return True
            else:
                total = 0
                for key in counts:
                    if condition_func(key):
                        total += 1
                        break
                return total > 0

        if prefix is None:
            prefix = []
        if len(prefix) != order:
            raise Exception(
                'the size of prefix_int must be equal to the order. Received: {}, {}'.format(prefix, order))

        if order == 0:
            counts = history
            return counts
        elif history.has_key(tuple(prefix)) and sum(
                history[tuple(prefix)].values()) > self.fallback_count and check_whenever_has_condition(
            history[tuple(prefix)]):
            counts = history[tuple(prefix)]
            return counts
        elif order == 1:
            # reduce to 0
            reduced_history = {}
            for key in history:
                for state in history[key]:
                    if reduced_history.has_key(state):
                        reduced_history[state] += history[key][state]
                    else:
                        reduced_history[state] = history[key][state]

            return self._get_counts(prefix=prefix[1:], _history=reduced_history, _order=order - 1,
                                    condition_func=condition_func)
        else:
            # fallback
            reduced_history = {}
            for key in history:
                reduced_key = tuple(key[1:])
                if reduced_history.has_key(reduced_key):
                    for state in history[key]:
                        if reduced_history[reduced_key].has_key(state):
                            reduced_history[reduced_key][state] += history[key][state]
                        else:
                            reduced_history[reduced_key][state] = history[key][state]
                else:
                    reduced_history[reduced_key] = deepcopy(history[key])
            return self._get_counts(prefix=prefix[1:], _history=reduced_history, _order=order - 1,
                                    condition_func=condition_func)

    def predict_next(self, prefix=None):
        counts = self._get_counts(prefix)
        return keymax(counts)

    def predict_next_condition(self, prefix, target, condition_func):
        counts = self._get_counts(prefix, condition_func=condition_func)
        condition_counts = {}
        for key in counts:
            if condition_func(key):
                condition_counts[key] = deepcopy(counts[key])

        # reduce to 1
        # print something is wrong
        # if len(condition_counts) == 0:
        #     condition_counts = deepcopy(counts)

        target_counts = {}

        for key in condition_counts:
            target_key = target(key)
            if target_counts.has_key(target_key):
                print "Something is wrong"
                target_counts[target_key] += condition_counts[key]
            else:
                target_counts[target_key] = deepcopy(condition_counts[key])

        if len(target_counts) > 0:
            return keymax(target_counts)
        else:
            raise RuntimeError('Sould not happen')
            return target(prefix[-1])

    def fit_predict(self, time_series, ratio_fit=0.9):
        predict_start = int(len(time_series) * ratio_fit)
        self.fit(time_series[0:predict_start])

        time_series_predict = []

        for i in range(predict_start, len(time_series)):
            prefix = time_series[i - self.order:i]
            # predict
            state_p = self.predict_next(prefix=prefix)
            time_series_predict.append(state_p)
            # train
            state = time_series[i]
            self._add_transition(prefix, state)

        time_series_truth = time_series[predict_start:]
        total = len(time_series_truth)
        right = 0.
        for i in range(total):
            if time_series_truth[i] == time_series_predict[i]:
                right += 1.
        score = right / total
        # print time_series_predict,time_series_truth
        # score = accuracy_score(time_series_truth, time_series_predict)
        return score, time_series_predict, time_series_truth

    def fit_predict_condition(self, time_series, target, condition, ratio_fit=0.9):
        predict_start = int(len(time_series) * ratio_fit)
        self.fit(time_series[0:predict_start])

        time_series_predict = []

        for i in range(predict_start, len(time_series)):
            prefix = time_series[i - self.order:i]
            state = time_series[i]
            # predict
            # print condition(state)
            target_state_p = self.predict_next_condition(prefix=prefix, target=target,
                                                         condition_func=lambda item: condition(item) == condition(
                                                             state))
            time_series_predict.append(target_state_p)
            # train
            self._add_transition(prefix, state)

        time_series_truth = map(target, time_series[predict_start:])

        total = len(time_series_truth)
        right = 0.
        for i in range(total):
            if time_series_truth[i] == time_series_predict[i]:
                right += 1.
        score = right / total
        # print time_series_predict,time_series_truth
        # score = accuracy_score(time_series_truth, time_series_predict)
        return score, time_series_predict, time_series_truth


class TemporalMarkovPredictor(Predictor):
    def __init__(self, order, fallback_count=2, id_offset=0, sep=4):
        super(TemporalMarkovPredictor, self).__init__()
        self.markovPredictor = MarkovPredictor1(order, fallback_count)
        self.id_offset = id_offset
        self.sep = sep

    def fit_predict(self, time_series, ratio_fit=0.9):
        target = lambda item: item[0]
        condition = lambda item: item[1]
        sep = lambda i: ((i + self.id_offset) % 24) // self.sep
        new_time_series = [(time_series[i], sep(i)) for i in range(len(time_series))]
        score, y_predict, y_truth = self.markovPredictor.fit_predict_condition(new_time_series, target=target,
                                                                               condition=condition,
                                                                               ratio_fit=ratio_fit)
        return score, y_predict, y_truth


class MarkovPredictor(IntStatePredictor):
    def __init__(self, order, fallback_count=2):
        super(MarkovPredictor, self).__init__()
        self.order = order
        self.fallback_count = fallback_count
        self.transition_counts = None

    def load(self, transition_counts, encoder):
        self.encoder = encoder
        self.transition_counts = transition_counts

    def bootstrap(self):
        super(MarkovPredictor, self).bootstrap()
        # transition_matrix
        order = self.order
        if self.transition_counts is None:
            self.transition_counts = np.zeros([len(self.classes_) for i in range(order + 1)])

    def _add_transition(self, prefix_int, state_int):
        if len(prefix_int) != self.order:
            raise Exception('the size of prefix_int must be equal to the order.')
        if self.order > 0:
            self.transition_counts[tuple(prefix_int)][state_int] += 1
        else:
            self.transition_counts[state_int] += 1.

        return self.transition_counts

    def _fit_int(self, int_series):
        # time_series must be list
        order = self.order
        for i in range(order, len(int_series)):
            state_int = int_series[i]
            prefix_int = int_series[i - order:i]
            self._add_transition(prefix_int, state_int)
        return self

    def _predict_next_int(self, prefix_int=None):
        if prefix_int is None:
            prefix_int = []
        if len(prefix_int) != self.order:
            raise Exception(
                'the size of prefix_int must be equal to the order. Received: {}, {}'.format(prefix_int, self.order))

        # predict
        order = self.order
        transition_counts = self.transition_counts

        if order == 0:
            counts = transition_counts
        else:
            counts = transition_counts[tuple(prefix_int)]
            # check if need fallback
            _transition_counts = transition_counts
            _reduced_prefix_int = prefix_int
            _order = self.order
            while counts.sum() <= self.fallback_count and _order > 0:
                _order -= 1
                _transition_counts = _transition_counts.sum(axis=0)
                if _order == 0:
                    counts = _transition_counts
                else:
                    _reduced_prefix_int = _reduced_prefix_int[1:]
                    counts = _transition_counts[tuple(_reduced_prefix_int)]
        return np.argmax(counts)

    def _fit_predict_int(self, int_series, ratio_fit=0.9):
        predict_start = int(len(int_series) * ratio_fit)
        self._fit_int(int_series[0:predict_start])

        _series_predict = []

        for i in range(predict_start, len(int_series)):
            # predict
            prefix_int = int_series[i - self.order:i]
            state_predict_int = self._predict_next_int(prefix_int)
            _series_predict.append(state_predict_int)

            # train increment
            state_int = int_series[i]
            self._add_transition(prefix_int, state_int)

        score = accuracy_score(_series_predict, int_series[predict_start:])
        return score, _series_predict, int_series[predict_start:]


class ALZPredictor(IntStatePredictor):
    """
    Active LeZi Predictor
    """

    def __init__(self):
        super(ALZPredictor, self).__init__()
        self.ALZ_tree = {'state': '', 'freq': 0, 'children': {}}
        self.dictionary = {}
        self.phase = []
        self.window = []
        self.time_series = []
        self.Max_LZ_length = 0

    def _update_ALZ_tree(self):
        window = self.window
        ALZ_tree = self.ALZ_tree
        for j in range(len(window)):
            key = tuple(window[j:])
            # locate places
            cur = ALZ_tree
            for i in range(len(key)):
                state = key[i]
                if not cur['children'].has_key(state):
                    cur['children'][state] = {'state': state, 'freq': 0, 'children': {}}
                cur = cur['children'][state]
            # add frequency
            cur['freq'] += 1

        ALZ_tree['freq'] = sum([ALZ_tree['children'][key]['freq'] for key in ALZ_tree['children']])
        self.ALZ_tree = ALZ_tree
        return self

    def _fit_state(self, state):
        v = [state]
        key = tuple(self.phase + v)
        if self.dictionary.has_key(key):
            self.phase = self.phase + v
        else:
            self.dictionary[key] = 1
            if self.Max_LZ_length < len(self.phase + v):
                self.Max_LZ_length = len(self.phase + v)
            self.phase = []

        self.window += v
        if len(self.window) > self.Max_LZ_length:
            # remove window[0]
            self.window = self.window[1:]

        self._update_ALZ_tree()
        self.time_series += v

    def _fit_int(self, time_series):
        for i in range(len(time_series)):
            self._fit_state(time_series[i])
        return self

    def _fit_predict_int(self, time_series, ratio_fit=0.9):
        predict_start = int(len(time_series) * ratio_fit)
        self._fit_int(time_series[0:predict_start])

        time_series_predict = []

        for i in range(predict_start, len(time_series)):
            # predict
            time_series_predict.append(self._predict_next_int())
            # train
            state = time_series[i]
            self._fit_state(state)

        time_series_truth = time_series[predict_start:]
        score = accuracy_score(time_series_truth, time_series_predict)
        return score, time_series_predict, time_series_truth

    def _predict_next_int(self, prefix=None):
        probabilities = {}
        fall_back_prob = 1.
        window = self.window

        for i in range(1, len(window)):
            suffix = window[i:]
            # find suffix root
            root = self.ALZ_tree
            # no need to check if has_key??
            for j in range(len(suffix)):
                state = suffix[j]
                root = root['children'][state]

            for key in root['children']:
                state = root['children'][key]['state']
                state_freq = root['children'][key]['freq']
                if not probabilities.has_key(state):
                    probabilities[state] = 0.
                probabilities[state] += fall_back_prob * 1.0 * state_freq / root['freq']

            sum_children_freqs = sum([root['children'][key]['freq'] for key in root['children']])
            fall_back_prob *= 1.0 * (root['freq'] - sum_children_freqs) / root['freq']

        # root
        root = self.ALZ_tree
        for key in root['children']:
            state = root['children'][key]['state']
            state_freq = root['children'][key]['freq']
            if not probabilities.has_key(state):
                probabilities[state] = 0.
            probabilities[state] += fall_back_prob * 1.0 * state_freq / root['freq']

        return max(probabilities, key=probabilities.get)


class ALZPredictor1(Predictor):
    """
    Active LeZi Predictor
    """

    def __init__(self):
        super(ALZPredictor1, self).__init__()
        self.ALZ_tree = {'state': '', 'freq': 0, 'children': {}}
        self.dictionary = {}
        self.phase = []
        self.window = []
        self.time_series = []
        self.Max_LZ_length = 0

    def _update_ALZ_tree(self):
        window = self.window
        ALZ_tree = self.ALZ_tree
        for j in range(len(window)):
            key = tuple(window[j:])
            # locate places
            cur = ALZ_tree
            for i in range(len(key)):
                state = key[i]
                if not cur['children'].has_key(state):
                    cur['children'][state] = {'state': state, 'freq': 0, 'children': {}}
                cur = cur['children'][state]
            # add frequency
            cur['freq'] += 1

        ALZ_tree['freq'] = sum([ALZ_tree['children'][key]['freq'] for key in ALZ_tree['children']])
        self.ALZ_tree = ALZ_tree
        return self

    def fit_state(self, state):
        v = [state]
        key = tuple(self.phase + v)
        if self.dictionary.has_key(key):
            self.phase = self.phase + v
        else:
            self.dictionary[key] = 1
            if self.Max_LZ_length < len(self.phase + v):
                self.Max_LZ_length = len(self.phase + v)
            self.phase = []

        self.window += v
        if len(self.window) > self.Max_LZ_length:
            # remove window[0]
            self.window = self.window[1:]

        self._update_ALZ_tree()
        self.time_series += v

    def fit(self, time_series):
        for i in range(len(time_series)):
            self.fit_state(time_series[i])
        return self

    def fit_predict(self, time_series, ratio_fit=0.9):
        predict_start = int(len(time_series) * ratio_fit)
        self.fit(time_series[0:predict_start])

        time_series_predict = []

        for i in range(predict_start, len(time_series)):
            # predict
            state_p = self.predict_next()
            time_series_predict.append(state_p)
            # train
            state = time_series[i]
            self.fit_state(state)

        time_series_truth = time_series[predict_start:]
        total = len(time_series_truth)
        right = 0.
        for i in range(total):
            if time_series_truth[i] == time_series_predict[i]:
                right += 1.
        score = right / total
        # print time_series_predict,time_series_truth
        # score = accuracy_score(time_series_truth, time_series_predict)
        return score, time_series_predict, time_series_truth

    def fit_predict_condition(self, time_series, target, condition, ratio_fit=0.9):
        predict_start = int(len(time_series) * ratio_fit)
        self.fit(time_series[0:predict_start])

        time_series_predict = []

        for i in range(predict_start, len(time_series)):
            state = time_series[i]
            # predict
            target_state_p = self.predict_next_condition(prefix=None, target=target,
                                                         condition_func=lambda item: condition(item) == condition(
                                                             state))
            time_series_predict.append(target_state_p)
            # train
            state = time_series[i]
            self.fit_state(state)

        time_series_truth = map(target, time_series[predict_start:])
        total = len(time_series_truth)
        right = 0.
        for i in range(total):
            if time_series_truth[i] == time_series_predict[i]:
                right += 1.
        score = right / total
        # print time_series_predict,time_series_truth
        # score = accuracy_score(time_series_truth, time_series_predict)
        return score, time_series_predict, time_series_truth

    def predict_next(self, prefix=None):
        counts = self._get_counts(prefix)
        return keymax(counts)

    def predict_next_condition(self, prefix, target, condition_func):
        counts = self._get_counts(prefix)
        condition_counts = {}
        for key in counts:
            if condition_func(key):
                condition_counts[key] = deepcopy(counts[key])

        target_counts = {}
        if len(condition_counts) == 0:
            condition_counts = deepcopy(counts)

        for key in condition_counts:
            target_key = target(key)
            if target_counts.has_key(target_key):
                target_counts[target_key] += condition_counts[key]
            else:
                target_counts[target_key] = deepcopy(condition_counts[key])

        return keymax(target_counts)

    def _get_counts(self, prefix=None):
        probabilities = {}
        fall_back_prob = 1.
        window = self.window

        for i in range(1, len(window)):
            suffix = window[i:]
            # find suffix root
            root = self.ALZ_tree
            # no need to check if has_key??
            for j in range(len(suffix)):
                state = suffix[j]
                root = root['children'][state]

            for key in root['children']:
                state = root['children'][key]['state']
                state_freq = root['children'][key]['freq']
                if not probabilities.has_key(state):
                    probabilities[state] = 0.
                probabilities[state] += fall_back_prob * 1.0 * state_freq / root['freq']

            sum_children_freqs = sum([root['children'][key]['freq'] for key in root['children']])
            fall_back_prob *= 1.0 * (root['freq'] - sum_children_freqs) / root['freq']

        # root
        root = self.ALZ_tree
        for key in root['children']:
            state = root['children'][key]['state']
            state_freq = root['children'][key]['freq']
            if not probabilities.has_key(state):
                probabilities[state] = 0.
            probabilities[state] += fall_back_prob * 1.0 * state_freq / root['freq']

        return probabilities


from math import ceil


class SPMPredictor(Predictor):
    def __init__(self, alpha):
        super(SPMPredictor, self).__init__()
        self.alpha = alpha
        if alpha <= 0. or alpha > 1.:
            raise EnvironmentError('Alpha need to be in (0,1].')

    def longest_suffix(self, time_series):
        matched_suffix = []
        i = 1
        while 1:
            suffix = time_series[-i:]
            goto_next = False
            for j in range(len(time_series) - len(suffix)):
                if time_series[j] == suffix[0] and time_series[j:j + len(suffix)] == suffix:
                    matched_suffix = suffix
                    goto_next = True
                    break
            if goto_next:
                i += 1
            else:
                break
        return matched_suffix

    def fit_predict(self, time_series, ratio_fit=0.9):
        predict_start = int(len(time_series) * ratio_fit)

        time_series_predict = []

        for i in range(predict_start, len(time_series)):
            prefix = time_series[0:i]
            # predict
            state_p = self.predict_next(prefix=prefix)
            time_series_predict.append(state_p)

        time_series_truth = time_series[predict_start:]
        total = len(time_series_truth)
        right = 0.
        for i in range(total):
            if time_series_truth[i] == time_series_predict[i]:
                right += 1.
        score = right / total
        # print time_series_predict,time_series_truth
        # score = accuracy_score(time_series_truth, time_series_predict)
        return score, time_series_predict, time_series_truth

    def fit_predict_condition(self, time_series, target, condition, ratio_fit=0.9):
        predict_start = int(len(time_series) * ratio_fit)

        time_series_predict = []

        for i in range(predict_start, len(time_series)):
            prefix = time_series[0:i]
            state = time_series[i]
            # predict
            target_state_p = self.predict_next_condition(prefix=prefix, target=target,
                                                         condition_func=lambda item: condition(item) == condition(
                                                             state))
            time_series_predict.append(target_state_p)

        time_series_truth = map(target, time_series[predict_start:])
        total = len(time_series_truth)
        right = 0.
        for i in range(total):
            if time_series_truth[i] == time_series_predict[i]:
                right += 1.
        score = right / total
        # print time_series_predict,time_series_truth
        # score = accuracy_score(time_series_truth, time_series_predict)
        return score, time_series_predict, time_series_truth

    def _get_counts(self, prefix=None):
        if prefix is None:
            prefix = []
        if len(prefix) == 0:
            raise Exception('Need history!')

        counts = {}
        suffix = self.longest_suffix(prefix)
        if len(suffix) == 0:
            counts[prefix[-1]] = 1
            return counts
        else:
            N = int(ceil(self.alpha * len(suffix)))
            matched = suffix[-N:]
            for i in range(len(prefix) - N):
                if prefix[i] == matched[0] and prefix[i:i + N] == matched:
                    candidate = prefix[i + N]
                    if counts.has_key(candidate):
                        counts[candidate] += 1
                    else:
                        counts[candidate] = 1

            return counts

    def predict_next(self, prefix=None):
        counts = self._get_counts(prefix)
        return keymax(counts)

    def predict_next_condition(self, prefix, target, condition_func):
        counts = self._get_counts(prefix)
        condition_counts = {}
        for key in counts:
            if condition_func(key):
                condition_counts[key] = deepcopy(counts[key])

        target_counts = {}
        if len(condition_counts) == 0:
            condition_counts = deepcopy(counts)

        for key in condition_counts:
            target_key = target(key)
            if target_counts.has_key(target_key):
                target_counts[target_key] += condition_counts[key]
            else:
                target_counts[target_key] = deepcopy(condition_counts[key])

        return keymax(target_counts)


class MlpPredictor(Predictor):
    def __init__(self, order=3, **kargs):
        super(MlpPredictor, self).__init__()
        self.order = order
        self.clf = MLPClassifier(**kargs)
        self.window = []
        self.classes = None

    def _fit_state(self, state, prefix):
        X_train = np.array([prefix])
        y_train = np.array([state])

        self.clf = self.clf.partial_fit(X_train, y_train, self.classes)
        return self

    def fit(self, time_series, init=True):
        N_samples = len(time_series) - self.order

        X_train = np.empty((N_samples, self.order))
        y_train = np.empty((N_samples,))

        # fill train samples
        for i in range(self.order, len(time_series)):
            pos = i - self.order
            X_train[pos, :] = time_series[i - self.order:i]
            y_train[pos] = time_series[i]

        # train
        if init:
            self.clf = self.clf.fit(X_train, y_train)
            self.classes = self.clf.classes_
        else:
            self.clf = self.clf.partial_fit(X_train, y_train, self.classes)
        return self

    def fit_predict(self, time_series, ratio_fit=0.9):
        predict_start = int(len(time_series) * ratio_fit)

        self.classes = np.unique(time_series)
        self.fit(time_series[0:predict_start], init=False)

        time_series_predict = []

        for i in range(predict_start, len(time_series)):
            prefix = time_series[i - self.order:i]
            # predict
            time_series_predict.append(self.predict_next(prefix))
            # train
            state = time_series[i]
            self._fit_state(state, prefix)

        time_series_truth = time_series[predict_start:]
        score = accuracy_score(time_series_truth, time_series_predict)
        return score, time_series_predict, time_series_truth

    def predict_next(self, prefix=None):
        if prefix is None:
            prefix = []
        if len(prefix) != self.order:
            raise Exception(
                'the size of prefix must be equal to the order. Received: {}, {}'.format(prefix, self.order))

        X_predict = np.array([prefix])
        return self.clf.predict(X_predict)[0]


class SklearnBasePredictor(Predictor):
    def __init__(self, sklearn_predictor, order=3, **kargs):
        super(SklearnBasePredictor, self).__init__()
        self.order = order
        self.clf = None
        self.predictor = sklearn_predictor
        self.classes = None
        self.kargs = kargs

    def _fit_state(self, state, prefix):
        X_train = np.array([prefix])
        y_train = np.array([state])

        self.clf = self.clf.partial_fit(X_train, y_train, self.classes)
        return self

    def _series2XY(self, time_series):
        N_samples = len(time_series) - self.order

        X = np.empty((N_samples, self.order))
        Y = np.empty((N_samples,))

        # fill train samples
        for i in range(self.order, len(time_series)):
            pos = i - self.order
            X[pos, :] = time_series[i - self.order:i]
            Y[pos] = time_series[i]

        return X, Y

    def fit(self, time_series, init=True):
        X_train, Y_train = self._series2XY(time_series)
        # train
        clf = self.predictor(**self.kargs)
        self.clf = clf.fit(X_train, Y_train)
        # if hasattr(clf, 'classes'):
        #     self.classes = clf.classes_
        return self

    def fit_predict(self, time_series, ratio_fit=0.9):
        predict_start = int(len(time_series) * ratio_fit)

        self.classes = np.unique(time_series)
        self.fit(time_series[0:predict_start], init=False)

        time_series_predict = []

        for i in range(predict_start, len(time_series)):
            prefix = time_series[i - self.order:i]
            # predict
            time_series_predict.append(self.predict_next(prefix))
            # train
            if hasattr(self.clf, 'partial_fit'):
                state = time_series[i]
                self._fit_state(state, prefix)
            else:
                self.fit(time_series[0:i])

        time_series_truth = time_series[predict_start:]
        score = accuracy_score(time_series_truth, time_series_predict)
        return score, time_series_predict, time_series_truth

    def predict_next(self, prefix=None):
        if prefix is None:
            prefix = []
        if len(prefix) != self.order:
            raise Exception(
                'the size of prefix must be equal to the order. Received: {}, {}'.format(prefix, self.order))

        X_predict = np.array([prefix])
        return self.clf.predict(X_predict)[0]

    pass


class RNNPredictor(Predictor):
    def __init__(self, order=3, type='RNN'):
        super(RNNPredictor, self).__init__()
        self.order = order
        RNNLayer = None
        if type == 'RNN':
            RNNLayer = SimpleRNN
        elif type == 'LSTM':
            RNNLayer = LSTM
        else:
            raise Exception('RNN model does not exist')

        self.RNNLayer = RNNLayer
        self.model = None

    def bootstrap(self, n_classes):
        # create and fit the LSTM network
        model = Sequential()
        model.add(LSTM(10, input_shape=(self.order, 1), return_sequences=False))
        model.add(Dense(100, activation='relu'))
        model.add(Dense(n_classes, activation='softmax'))
        model.compile(loss='categorical_crossentropy',
                      optimizer='adam',
                      metrics=['accuracy'])
        self.model = model
        return self

    def transform_data(self, time_series):
        look_back = self.order
        dataset = np.array([time_series]).T
        dataX, dataY = [], []
        for i in range(len(dataset) - look_back - 1):
            a = dataset[i:(i + look_back), 0]
            dataX.append(a)
            dataY.append(dataset[i + look_back, 0])

        return np.array(dataX), to_categorical(np.array(dataY))

    def fit(self, time_series):
        # TODO
        raise NotImplementedError

    def fit_predict(self, time_series, ratio_fit=0.9):
        # fix_bug
        n_classes = len(np.unique(time_series[self.order:]))
        self.bootstrap(n_classes)

        X_all, Y_all = self.transform_data(time_series)

        predict_start = int(len(time_series) * ratio_fit)
        X_train = X_all[0:predict_start, :]
        Y_train = Y_all[0:predict_start, :]
        X_test = X_all[predict_start:, :]
        Y_test = Y_all[predict_start:, :]

        # reshape input to be [samples, time steps, features]
        X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
        X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

        # initial fit model
        self.model.fit(X_train, Y_train, epochs=15, batch_size=24 * 2, verbose=0)

        Y_test_int = np.argmax(Y_test, axis=1)
        Y_predict_test_int = np.copy(Y_test_int)
        # predict
        for i in range(Y_test.shape[0]):
            if i % 24 == 0:
                self.model.fit(X_test[i - 24:i, :], Y_test[i - 24:i, :], epochs=2, batch_size=24, verbose=0)
            y_int = np.argmax(self.model.predict(X_test[i:i + 1, :])[0])
            Y_predict_test_int[i] = y_int

        return accuracy_score(Y_test_int, Y_predict_test_int), Y_predict_test_int, Y_test_int
        # return score, time_series_predict, time_series_truth

    def predict_next(self, prefix=None):
        # TODO
        raise NotImplementedError


# Prediction Analyzer
class PredictionAnalyzer(Analyzer):
    def __init__(self, *args):
        super(PredictionAnalyzer, self).__init__(*args)

    def run(self):
        dt = self.dt
        res = pd.DataFrame([], index=dt.index,
                           columns=self.assign_tasks().keys())
        for index, row in dt.iterrows():
            time_series = row.tolist()

            tasks = self.assign_tasks()
            for key in tasks:
                predictor = tasks[key]
                score, _p, _t = predictor.fit_predict(time_series, ratio_fit=0.9)
                res.loc[index, key] = score

        self.rs = res
        return self.rs

    def assign_tasks(self):
        raise NotImplementedError('Implement some tasks!')


class MAEPredictionAnalyzer(PredictionAnalyzer):
    def __init__(self, *args):
        super(MAEPredictionAnalyzer, self).__init__(*args)

    def assign_tasks(self):
        tasks = dict()
        # tasks['P_M1'] = MarkovPredictor1(order=1, fallback_count=2)
        tasks['P_Markov-O2'] = MarkovPredictor1(order=2, fallback_count=0)
        tasks['P_Markov-O2old'] = MarkovPredictor(order=2, fallback_count=0)
        for sep in [6, 8, 12]:
            for o in [2, 3, 4]:
                tasks['P_Markov-O%dSep%d' % (o, sep)] = TemporalMarkovPredictor(order=o, fallback_count=0, sep=sep)
        # tasks['P_Markov-O2New8'] = TemporalMarkovPredictor(order=3, fallback_count=0, sep=8)
        # tasks['P_Markov-O2New12'] = TemporalMarkovPredictor(order=2, fallback_count=2, sep=12)
        # tasks['P_SPM-A20'] = SPMPredictor(alpha=0.20)
        # tasks['P_SPM-A25'] = SPMPredictor(alpha=0.25)
        # tasks['P_SPM-A30'] = SPMPredictor(alpha=0.30)
        # tasks['P_ALZ'] = ALZPredictor1()
        # tasks['P_SVC_O2'] = SklearnBasePredictor(SVC, order=2)
        # tasks['P_SVC_O4'] = SklearnBasePredictor(SVC, order=4)
        # tasks['P_DTree_O2'] = SklearnBasePredictor(DecisionTreeClassifier, order=2)
        # tasks['P_DTree_O4'] = SklearnBasePredictor(DecisionTreeClassifier, order=4)
        # tasks['P_ALZ'] = ALZPredictor()

        # for order in range(0, 4):
        #     tasks['P_Markov%d' % order] = MarkovPredictor(order=order, fallback_count=10)
        return tasks


class DPPredictionAnalyzer(PredictionAnalyzer):
    def __init__(self, *args):
        super(DPPredictionAnalyzer, self).__init__(*args)

    def assign_tasks(self):
        tasks = dict()
        for order in range(2, 4):
            # For MLP, it is meaningless to use an order more than 3.
            # tasks['P_MLP%d' % order] = MlpPredictor(order=order)
            tasks['P_SimpleRNN%d' % order] = RNNPredictor(order=order, type='RNN')
            tasks['P_LSTM%d' % order] = RNNPredictor(order=order, type='LSTM')
        return tasks


# Joint Prediction Analyzer
# Prediction Analyzer
class JointPredictionAnalyzer(Analyzer):
    def __init__(self, dt, top_k_loc=1):
        """
        
        Args:
            dt: tuple (data_volumes, data_locations). The same index is shared by two data. 
        """
        super(JointPredictionAnalyzer, self).__init__(dt)
        self.dt, self.dt_loc = dt  # self.dt: DataFrame of volumes, self.dt_loc: DataFrame of locations
        self.top_k_loc = top_k_loc
        print "Top K Loc", self.top_k_loc
        if not self.dt.index.equals(self.dt_loc.index):
            print self.dt.index
            print self.dt_loc.index
            raise Exception('In tuple dt=(data_volumes, data_locations). The same index must be shared by two data!')

    def run(self):
        dt_volumes = self.dt
        dt_locations = self.dt_loc

        res = pd.DataFrame([], index=dt_volumes.index,
                           columns=[])

        for tuple1, tuple2 in izip(dt_volumes.iterrows(), dt_locations.iterrows()):
            index, rowV = tuple1
            _index, rowL = tuple2

            if index != _index:
                raise Exception('User error!')

            locs, counts = np.unique(rowL, return_counts=True)
            # top 2 locations
            N = min(self.top_k_loc, len(locs))
            # print np
            top_locs = map(lambda v: v[0], sorted(zip(locs, counts), key=lambda v: v[1], reverse=True)[0:N])
            _rowL = [str(loc) if loc in top_locs else 'other' for loc in rowL.tolist()]
            # print len(np.unique(rowV)), len(np.unique(rowL))
            # time_series = [{'v': v, 'l': l} for v, l in zip(rowV, _rowL)]
            time_series = [(v, l) for v, l in zip(rowV, _rowL)]
            rowV = rowV.tolist()

            tasksVL = self.assign_tasks()
            for key in tasksVL:
                score, predicted_series, real_series = deepcopy(tasksVL[key]).fit_predict(time_series, ratio_fit=0.9)

                res.loc[index, key + '_VL_joint'] = score

                score_V, predicted_series_V, real_series_V = deepcopy(tasksVL[key]).fit_predict(rowV, ratio_fit=0.9)
                score_L, predicted_series_L, real_series_L = deepcopy(tasksVL[key]).fit_predict(_rowL, ratio_fit=0.9)

                res.loc[index, key + '_V_alone'] = score_V

                res.loc[index, key + '_L_alone'] = score_L

                res.loc[index, key + '_V_joint'] = accuracy_score(map(lambda v: v[0], predicted_series),
                                                                  map(lambda v: v[0], real_series))

                res.loc[index, key + '_L_joint'] = accuracy_score(map(lambda v: v[1], predicted_series),
                                                                  map(lambda v: v[1], real_series))

                _predicted_series = [(v, l) for v, l in zip(predicted_series_V, predicted_series_L)]
                score_VL = 0.
                for i in range(len(_predicted_series)):
                    p = _predicted_series[i]
                    q = real_series[i]
                    if p[0] == q[0] and p[1] == q[1]:
                        score_VL += 1.

                res.loc[index, key + '_VL_alone'] = score_VL / len(_predicted_series)

                if hasattr(tasksVL[key], 'fit_predict_condition'):
                    target = lambda item: item[0]
                    condition = lambda item: item[1]
                    score, predicted_series, real_series = deepcopy(tasksVL[key]).fit_predict_condition(time_series,
                                                                                                        target,
                                                                                                        condition,
                                                                                                        ratio_fit=0.9)
                    res.loc[index, key + '_V|L'] = score

                    target = lambda item: item[1]
                    condition = lambda item: item[0]
                    score, predicted_series, real_series = deepcopy(tasksVL[key]).fit_predict_condition(time_series,
                                                                                                        target,
                                                                                                        condition,
                                                                                                        ratio_fit=0.9)
                    res.loc[index, key + '_L|V'] = score

        self.rs = res
        return self.rs

    def assign_tasks(self):
        # raise NotImplementedError('Implement some tasks!')
        return {
            # 'P_Markov1': MarkovPredictor1(order=1, fallback_count=3),
            'P_Markov2': MarkovPredictor1(order=2, fallback_count=0),
            # 'P_ALZ': ALZPredictor1(),
            # 'P_SPM-25': SPMPredictor(alpha=0.25)
            # 'P_Markov3': MarkovPredictor(order=3, fallback_count=3),
            # 'P_ALZ': ALZPredictor()
        }


MAEJointPredictionAnalyzer = JointPredictionAnalyzer
