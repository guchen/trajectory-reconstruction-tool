from functools import wraps


def cached(func):
    """
    Wrap to add caching on a function. 
    
    Cache results of function and use tuple(input_args) as key.
    
    Parameters
    ----------
    func : function

    Returns
    -------
    func_with_cache: function
    """
    memo = {}

    @wraps(func)
    def function_with_cache(*args):
        if args in memo:
            return memo[args]
        else:
            rv = func(*args)
            memo[args] = rv
            return rv

    return function_with_cache
