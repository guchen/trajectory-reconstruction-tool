# -*- coding: utf-8 -*-
from __future__ import print_function, division

from math import radians, sin, cos, asin, sqrt, pow

import numpy as np
import numba


@numba.jit
def centroid(pts):
    """
    Compute the centroid of mass of points

    Parameters
    ----------
    pts : 2D-array or list
        A list of tuples (latitude, longitude).
        or A 2D numpy array. Row: point, Column: latitude, longitude

    Returns
    -------
    pt: tuple
        a centroid point in (latitude, longitude)

    >>> centroid([(1,1),(2,2),(5,15),(11,20)])
    (4.75, 9.5)
    >>> centroid(np.ones((100,2)))
    (1.0, 1.0)
    """

    # lat_mean = np.mean([pt[0] for pt in pts])
    # lon_mean = np.mean([pt[1] for pt in pts])

    pts = np.array(pts)
    if pts.shape[1] != 2:
        raise ValueError('Pts must be an 2D Nx2 array or a list of tuples (lat,lon).')

    lat_mean, lon_mean = np.mean(pts, axis=0)
    return lat_mean, lon_mean


def distance(u, v):
    """
    Compute the great circle distance between two points on the earth (specified in decimal degrees)

    Parameters
    ----------
    u : tuple
        a point in (latitude, longitude)
    v : tuple
        a point in (latitude, longitude)

    Returns
    -------
    geo_dist: float
        the distance between u and v in kilometers.
    """
    RADIUS_EARTH = 6367  # kilometers

    lat1 = u[0]
    lon1 = u[1]
    lat2 = v[0]
    lon2 = v[1]
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    km = RADIUS_EARTH * c
    return km


def rg(pts):
    """
    Compute the radius of gyration of mass of points.

    https://en.wikipedia.org/wiki/Radius_of_gyration

    Parameters
    ----------
    pts : 2D-array or list
        A list of tuples (latitude, longitude).
        or A 2D numpy array. Row: point, Column: latitude, longitude

    Returns
    -------
    v : radius of gyration
    """

    r_mean = centroid(pts)
    rg2 = np.mean([distance(r_mean, pt) ** 2 for pt in pts])

    return sqrt(rg2)


__test__ = {'centroid': centroid.__doc__}
if __name__ == "__main__":
    import doctest

    doctest.testmod(verbose=1)

    import sys

    mod = sys.modules.get('__main__')
    print(mod)
    print(dir(mod))
