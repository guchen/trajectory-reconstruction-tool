# -*- coding: utf-8 -*-
# predictability analysis for the lcn task.
# maybe for the journal
import numpy as np
import pandas as pd

from qbmn import entropy
from qbmn.util.geo import rg


def measure_user_predictability_volume(row_args):
    """
    >>> res = measure_user_predictability_volume([pd.Series([0,1,2,3,4,5,6,7,8,9,10],name='ABC')])
    >>> res.name
    'ABC'
    >>> res['N']
    11.0
    """
    row = row_args[0]
    name = row.name
    rs = entropy.compute_entropy_related_stats(row.values)
    return pd.Series(dict({
        'pred_rand': entropy.predictability_max(rs['etp_rand'], rs['N']),
        'pred_unc': entropy.predictability_max(rs['etp_unc'], rs['N']),
        'pred_unc_min': entropy.predictability_min(rs['etp_unc'], rs['N']),
        'pred_est': entropy.predictability_max(rs['etp_est'], rs['N']),
        'pred_est_min': entropy.predictability_min_erate(rs['etp_est'], rs['N']),
        'pred_unc_pure': entropy.predictability_max(rs['etp_unc_pure'], rs['N'] - 1),
        'pred_unc_pure_min': entropy.predictability_min(rs['etp_unc_pure'], rs['N'] - 1)
    }, **rs), name=name)


def measure_user_regularity_volume(row_args):
    '''
    R: overall probability_max
    R_Weekday: weekdays
    R_Weekend: weekends
    R_H_X: hourly probability_max
    R_WH_X: weekly and hourly probability_max
    '''
    row = row_args[0]
    name = row.name
    R_WH = row.groupby(lambda d: 'R_WH_%d' % (d.weekday() * 24 + d.hour)).apply(
        lambda seq: entropy.probability_max(seq.values))
    N_WH = row.groupby(lambda d: 'N_WH_%d' % (d.weekday() * 24 + d.hour)).apply(
        lambda seq: len(np.unique(seq.values)))
    R_H = row.groupby(lambda d: 'R_H_%d' % d.hour).apply(
        lambda seq: entropy.probability_max(seq.values))
    N_H = row.groupby(lambda d: 'N_H_%d' % d.hour).apply(
        lambda seq: len(np.unique(seq.values)))
    R_W = row.groupby(lambda d: 'R_Weekday' if d.weekday() <= 4 else 'R_Weekend').apply(
        lambda seq: entropy.probability_max(seq.values))
    N_W = row.groupby(lambda d: 'N_Weekday' if d.weekday() <= 4 else 'R_Weekend').apply(
        lambda seq: len(np.unique(seq.values)))
    R = {
        'R': entropy.probability_max(row.values)
    }
    R.update(R_H)
    R.update(R_WH)
    R.update(R_W)
    R.update(N_H)
    R.update(N_WH)
    R.update(N_W)
    row_v = row[row != 0]
    VR_WH = row_v.groupby(lambda d: 'VR_WH_%d' % (d.weekday() * 24 + d.hour)).apply(
        lambda seq: entropy.probability_max(seq.values))
    VR_H = row_v.groupby(lambda d: 'VR_H_%d' % d.hour).apply(
        lambda seq: entropy.probability_max(seq.values))
    VR_W = row_v.groupby(lambda d: 'VR_Weekday' if d.weekday() <= 4 else 'VR_Weekend').apply(
        lambda seq: entropy.probability_max(seq.values))
    VR = {
        'VR': entropy.probability_max(row_v.values)
    }
    R.update(VR)
    R.update(VR_W)
    R.update(VR_H)
    R.update(VR_WH)
    return pd.Series(R, name=name)


def measure_user_predictability_location(row_args):
    row = row_args[0]
    name = row.name
    _list = row.tolist()
    _list_loc_only = row[row != '?'].tolist()

    N = np.unique(_list_loc_only) - 1
    pts = [pt.split('|') for pt in _list_loc_only]
    pts = [(float(pt[0]), float(pt[1])) for pt in pts]

    etp_loc, cov = entropy.entropy_rate_location(_list)

    rs = {
        'etp_unc': entropy.entropy_tunc(_list_loc_only),
        'etp_rand': entropy.entropy_rand(_list_loc_only),
        'etp_est': entropy.entropy_rate_location(_list_loc_only),
        'cov_etp_est': cov,
        'rg': rg(pts),
        'N': N
    }

    return pd.Series(dict({
        'pred_rand': entropy.predictability_max(rs['etp_rand'], rs['N']),
        'pred_unc': entropy.predictability_max(rs['etp_unc'], rs['N']),
        'pred_unc_min': entropy.predictability_min(rs['etp_unc'], rs['N']),
        'pred_est': entropy.predictability_max(rs['etp_est'], rs['N']),
        'pred_est_min': entropy.predictability_min_erate(rs['etp_est'], rs['N']),
    }, **rs), name=name)


def measure_user_predictability_volume_location(row_args):
    pass

if __name__ == '__main__':
    import doctest

    doctest.testmod(verbose=1)
