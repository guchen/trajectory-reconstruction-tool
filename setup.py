#!/usr/bin/env python
import os
try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

    def find_packages(where='.'):
        # os.walk -> list[(dirname, list[subdirs], list[files])]
        return [folder.replace("/", ".").lstrip(".")
                for (folder, _, fils) in os.walk(where)
                if "__init__.py" in fils]

import sys
from io import open as io_open

src_dir = os.path.abspath(os.path.dirname(__file__))

README = ''
fndoc = os.path.join(src_dir, 'README.md')
with io_open(fndoc, mode='r', encoding='utf-8') as fd:
    README = fd.read()

setup(
    name='location-infer',
    version='0.0.1',
    description='Tools/Function to infer missing mobility information',
    long_description=README,
    url='https://gitlab.inria.fr/guchen/trajectory-reconstruction-tool',
    maintainer='Guangshuo Chen',
    maintainer_email='guangshuo.chen@gmail.com',
    license='MIT License',
    platforms=['any'],
    packages=['inferLoc'],
    provides=['inferLoc'],
    package_data={'inferLoc': ['README.md', 'LICENCE', 'requirements.txt']},
    python_requires='>=2.6, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, !=3.5.*',
)
